<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        \App\Models\User::factory()->create([
            'name' => 'Alexander Drobkov',
            'email' => 'work@sanes.net.ru',
            'password' => '$2y$12$Rnl3Uvu3P11N562P877FkuRW8XXaMZPxOGiQAaO3bNlPeB0IuzsmG'
        ]);
    }
}
