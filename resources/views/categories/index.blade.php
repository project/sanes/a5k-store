@extends('app')
@section('content')
@include('categories.create')
<div class="uk-child-width-1-3@l uk-child-width-1-2@m uk-grid-small" uk-grid>
	@foreach($nodes as $node)
	<div>
		<ul class="uk-nav uk-nav-default">
			<li class="uk-nav-header">{{ $node->title }}</li>
			@foreach($node->descendants as $category)
			<li>{{ $category->title }}</li>
			@endforeach
		</ul>
	</div>
	@endforeach
</div>
@endsection