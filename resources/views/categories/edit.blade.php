@extends('app')
@section('content')
<h3>Изменить категорию</h3>
<form action="{{ route('categories.update', $category) }}" class="uk-form-stacked" method="post">
	<div class="uk-margin">
		@csrf
		@method('put')
		<div class="uk-form-controls">
		<label for="title" class="uk-form-label">Название категории</label>
			<input type="text" class="uk-input" id="title" name="title" required minlength="3" maxlength="50" value="{{ $category->title }}">
		</div>
	</div>
	<div class="uk-margin">
		<div class="uk-form-controls">
			<label for="node" class="uk-form-label">Родительская категория</label>
			<select name="node" id="node" class="uk-select">
				<option>----------------------------</option>
				@foreach($nodes as $node)
				@if($node->isRoot())
				<option value="{{ $node->id }}">{{ $node->title }}</option>
				@endif
				@endforeach
			</select>
		</div>
	</div>
	<div class="uk-margin">
		<button class="uk-button uk-button-default" type="submit">Сохранить</button>
	</div>
</form>
@endsection