@extends('app')
@section('content')
<div class="uk-grid-small" uk-grid>
	<div class="uk-width-expand">
		<h3>{{ $category->title }}</h3>
	</div>
	<div class="uk-width-auto">
		<a class="uk-icon-button" href="{{ route('categories.edit', $category) }}" uk-icon="file-edit"></a>
		@if(!$category->isRoot())<a class="uk-icon-button" href="#create" uk-icon="plus" uk-toggle></a>@endif
	</div>
</div>
@if(!$category->isRoot())
<table class="uk-table uk-table-middle uk-table-small uk-table-hover uk-table-striped uk-text-small">
	<thead>
		<tr>
			{{-- <th class="uk-table-shrink">ID</th> --}}
			<th>Наименование</th>
			<th class="uk-table-shrink">Цена</th>
			<th class="uk-table-shrink">Обновление</th>
			<th style="min-width: 20px;"></th>
		</tr>
	</thead>
	<tbody>
		@foreach($products as $product)
		<tr>
			{{-- <td class="">{{ $product->sku }}</td> --}}
			<td class="uk-width-expand uk-table-link"><a href="{{ route('products.edit', $product) }}" class="uk-link-reset">{{ $product->title }}</a></td>
			<td class="uk-text-nowrap">{{ number_format($product->price, 0, '', ' ') }} ₽</td>
			<td>{{ $product->updated_at->format('d.m.Y') }}</td>
			<td><a href="https://google.ru/search?q={{ str_replace(" ", "+", $product->title) }}" class="uk-icon-link" uk-icon="search" target="_blank"></a></td>
		@endforeach
	</tbody>
</table>
{{ $products->withQueryString()->links('pagination') }}
@include('products.create')
@endif
@endsection