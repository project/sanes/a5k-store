<ul class="uk-nav uk-nav-default">
@foreach($nodes as $node)
	<li class="uk-nav-header"><a href="{{ route('categories.show', $node) }}" class="uk-link-reset">{{ $node->title }}</a></li>
	@foreach($node->descendants as $category)
	<li><a href="{{ route('categories.show', $category) }}">{{ $category->title }}</a></li>
	@endforeach
@endforeach
</ul>
<div class="uk-margin">
	<a href="{{ route('categories.create') }}" class="uk-icon-link uk-icon-button" uk-icon="plus"></a>
</div>