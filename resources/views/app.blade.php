<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>{{ config('app.name') }}</title>
	<link rel="stylesheet" href="/css/uikit.min.css">
	<script src="/js/uikit.min.js"></script>
	<script src="/js/uikit-icons.min.js"></script>
	<script src="https://unpkg.com/htmx.org@1.9.11" crossorigin="anonymous"></script>
</head>
<body hx-boost="true">
	<div class="uk-container uk-container-large">
		<div class="uk-navbar" uk-navbar>
			<div class="uk-navbar-left">
				<a href="/" class="uk-logo uk-navbar-item">{{ config('app.name') }}</a>
			</div>
		</div>
		<hr class="uk-margin-remove-top">
		<div class="uk-grid-medium uk-grid-divider" uk-grid>
			<div class="uk-width-1-4@l uk-width-1-5@xl">
				@include('sidebar')
			</div>
			<div class="uk-width-3-4@l uk-width-4-5@xl">
				@yield('content')
			</div>
		</div>
	</div>
	@yield('js')
</body>
</html>