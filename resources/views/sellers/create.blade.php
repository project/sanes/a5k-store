
<div id="create-seller" class="uk-flex-top" uk-modal>
    <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">
        <button class="uk-modal-close-default" type="button" uk-close></button>
		<h4>Добавить аналог</h4>
		<form action="{{ route('sellers.store') }}" class="uk-form-stacked" method="post">
			@csrf
			<input type="number" class="uk-hidden" name="product_id" value="{{ $product->id }}">
			<div class="uk-margin">
				<div class="uk-form-controls">
					<label for="title" class="uk-form-label">Наименование</label>
					<input type="text" class="uk-input" id="title" name="title" required autofocus>
				</div>
			</div>
			<div class="uk-child-width-1-2@m" uk-grid>			
				<div>
					<div class="uk-form-controls">
						<label for="price" class="uk-form-label">Цена</label>
						<input type="number" class="uk-input" id="price" name="price" min="0" required>
					</div>				
				</div>
				<div>
					<div class="uk-form-controls">
						<label for="seller" class="uk-form-label">Продавец</label>
						<input type="text" class="uk-input" id="seller" name="seller" required>
					</div>				
				</div>
			</div>
			<div class="uk-margin">
				<div class="uk-form-controls">
					<label for="link" class="uk-form-label">Ссылка</label>
					<input type="text" class="uk-input" id="link" name="link" required>
				</div>				
			</div>
			<div class="uk-margin">
				<button class="uk-button uk-button-default" type="submit">Добавить</button>
			</div>			
		</form>
    </div>
</div>


