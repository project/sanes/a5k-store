@extends('app')
@section('content')
<h4>{{ $product->title }}</h4>
<form action="{{ route('products.update', $product) }}" class="uk-form-stacked" method="post">
	@csrf
	@method('put')
	<div class="uk-margin">
		<div class="uk-form-controls">
			<label for="title" class="uk-form-label">Наименование</label>
			<input type="text" name="title" id="title" class="uk-input" value="{{ $product->title }}" required>
		</div>
	</div>
	<div class="uk-margin">
		<div class="uk-child-width-1-2" uk-grid>
			<div>
				<div class="uk-form-controls">
					<label for="sku" class="uk-form-label">ID</label>
					<input type="number" class="uk-input" id="sku" name="sku" value="{{ $product->sku }}">
				</div>
			</div>
			<div>
				<div class="uk-form-controls">
					<label for="price" class="uk-form-label">Цена</label>
					<input type="number" class="uk-input" id="price" name="price" min="0" value="{{ $product->price }}">
				</div>
			</div>
		</div>
	</div>
	<div class="uk-margin">

		<button class="uk-button uk-button-default uk-margin-right" type="submit">Сохранить</button>
		<a href="{{ route('categories.show', $product->category) }}" class="uk-button uk-button-primary uk-margin-right">{{ $product->category->title }}</a>
		<a href="https://google.ru/search?q={{ str_replace(" ", "+", $product->title) }}" class="uk-icon-link" uk-icon="search" target="_blank"></a>
	</div>
</form>
<hr>
<div uk-grid>
	<div class="uk-width-expand">
		<h4>Аналоги</h4>
	</div>
	<div class="uk-width-auto">
		<a class="uk-icon-button" href="#create-seller" uk-icon="plus" uk-toggle></a>
	</div>
</div>
<table class="uk-table uk-table-middle uk-table-small uk-table-hover uk-table-striped uk-text-small">
	<thead>
		<tr>
			<th class="uk-width-expand">Наименование</th>
			<th class="uk-table-shrink">Продавец</th>
			<th class="uk-table-shrink">Цена</th>
			<th style="min-width: 44px;"></th>
		</tr>
	</thead>
	<tbody>
		@foreach($product->sellers as $seller)
		<tr>
			<td class="uk-table-link"><a href="{{ $seller->link }}" class="uk-link-reset" target="_blank">{{ $seller->title }}</a></td>
			<td class="uk-text-nowrap">{{ $seller->seller }}</td>
			<td class="uk-text-nowrap">{{ number_format($seller->price, 0, '', ' ') }} ₽</td>
			<td>
				<a onclick="destroy('{{ route('sellers.destroy', $seller) }}')" class="uk-icon-link" uk-icon="trash"></a>
				<a href="https://google.ru/search?q={{ str_replace(" ", "+", $seller->title) }}" class="uk-icon-link" uk-icon="search" target="_blank"></a>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
@include('sellers.create')
<form method="post" class="uk-hidden" id="destroy">
	@csrf
	@method('delete')
</form>
@endsection
@section('js')
@if($product->sellers->count() > 0)
<script>
	function destroy(url){
	  let del = confirm('Удалить?');
	  if(del){

			form = document.querySelector("#destroy")
			form.setAttribute("action", url)
			form.submit()
		}
	}
</script>
@endif
@endsection