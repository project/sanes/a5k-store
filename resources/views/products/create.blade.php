<div id="create" class="uk-flex-top" uk-modal>
    <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical">
        <button class="uk-modal-close-default" type="button" uk-close></button>
		<h4>Добавить оборудование</h4>
		<form action="{{ route('products.store') }}" class="uk-form-stacked" method="post">
			@csrf
			<input type="text" class="uk-hidden" name="category" value="{{ $category->id }}">
			<div class="uk-margin">
				<div class="uk-form-controls">
					<label for="title" class="uk-form-label">Наименование</label>
					<input type="text" name="title" id="title" class="uk-input" required autofocus>
				</div>
			</div>
			<div class="uk-margin">
				<div class="uk-child-width-1-2" uk-grid>
					<div>
						<div class="uk-form-controls">
							<label for="sku" class="uk-form-label">ID</label>
							<input type="number" class="uk-input" id="sku" name="sku" min="1">
						</div>
					</div>
					<div>
						<div class="uk-form-controls">
							<label for="price" class="uk-form-label">Цена</label>
							<input type="number" class="uk-input" id="price" name="price" min="0" value="0">
						</div>
					</div>
				</div>
			</div>
			<div class="uk-margin">
				<button class="uk-button uk-button-default" type="submit">Добавить</button>
			</div>
		</form>
    </div>
</div>











