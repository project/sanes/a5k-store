<?php

namespace App\Http\Controllers;

use App\Models\Seller;
use Illuminate\Http\Request;use Spatie\RouteAttributes\Attributes\Get;
use Spatie\RouteAttributes\Attributes\Post;
use Spatie\RouteAttributes\Attributes\Put;
use Spatie\RouteAttributes\Attributes\Delete;
use Spatie\RouteAttributes\Attributes\Prefix;

#[Prefix('sellers')]
class SellerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    #[Post('/store', name: "sellers.store")]

    public function store(Request $request)
    {
        Seller::create(['title' => $request->title, 'price' => $request->price, 'seller' => $request->seller, 'link' => $request->link, 'product_id' => $request->product_id, 'user_id' => 1]);

        return redirect()->route('products.edit', $request->product_id);
    }

    /**
     * Display the specified resource.
     */
    public function show(Seller $seller)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Seller $seller)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Seller $seller)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    #[Delete('/{seller}store', name: "sellers.destroy")]

    public function destroy(Seller $seller)
    {
        $seller->delete();

        return redirect()->back();
    }
}
