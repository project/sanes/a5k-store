<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Spatie\RouteAttributes\Attributes\Get;
use Spatie\RouteAttributes\Attributes\Post;
use Spatie\RouteAttributes\Attributes\Put;
use Spatie\RouteAttributes\Attributes\Prefix;

#[Prefix('categories')]
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    #[Get('/sidebar', name: "sidebar")]
    public function sidebar()
    {
        // $categories = Category::with('ancestors')->get();
        $nodes = Category::whereIsRoot()->get();
        return view('sidebar', compact('nodes'));
    }

    #[Get('/', name: "categories.index")]
    public function index()
    {
        // $categories = Category::with('ancestors')->get();
        $nodes = Category::whereIsRoot()->get();

        return view('categories.index', compact('nodes'));
    }

    /**
     * Show the form for creating a new resource.
     */
    #[Get('/create', name: "categories.create")]
    public function create()
    {
        $nodes = Category::whereIsRoot()->get();

        return view('categories.create', compact('nodes'));  
    }

    /**
     * Store a newly created resource in storage.
     */
    #[Post('/store', name: "categories.store")]
    public function store(Request $request)
    {
        if ($request->node) {
            $node = Category::find($request->node);
            Category::create(['title' => $request->title], $node);
        }
        else{
            Category::create(['title' => $request->title]);
        }

        return redirect()->route('categories.index');      
    }

    /**
     * Display the specified resource.
     */
    #[Get('/{category}', name: "categories.show")]
    public function show(Category $category)
    {

        $products = Product::where('category_id', $category->id)->orderByDesc('updated_at')->paginate(50);
        // dd($category);
        return view('categories.show', compact('category', 'products'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    #[Get('/{category}/edit', name: "categories.edit")]
    public function edit(Category $category)
    {
        $nodes = Category::whereIsRoot()->get();

        return view('categories.edit', compact('nodes', 'category')); 
    }

    /**
     * Update the specified resource in storage.
     */
    #[Put('/{category}', name: "categories.update")]
    public function update(Request $request, Category $category)
    {
        $node = Category::find($request->node);
        $category->update(['title' => $request->title]);
        if ($request->node) {
            $category->parent()->associate($node)->save();
        }
        else
        {
            $category->makeRoot();
        }        
        return redirect()->route('categories.show', $category);
    }

    /**
     * Remove the specified resource from storage.
     */
    // #[Get('/{id}', name: "categories.destroy")]
    // public function destroy(Category $category)
    // {
    //     //
    // }
}
