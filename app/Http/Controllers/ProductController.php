<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Spatie\RouteAttributes\Attributes\Get;
use Spatie\RouteAttributes\Attributes\Post;
use Spatie\RouteAttributes\Attributes\Put;
use Spatie\RouteAttributes\Attributes\Prefix;

#[Prefix('products')]
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    #[Get('/create', name: "products.create")]
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    #[Post('/store', name: "products.store")]
    public function store(Request $request)
    {
        $product = Product::create(['title' => $request->title, 'sku' => $request->sku, 'price' => $request->price, 'category_id' => $request->category, 'user_id' => 1]);

        return redirect()->route('products.edit', $product);
    }

    /**
     * Display the specified resource.
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    #[Get('/{product}/edit', name: "products.edit")]
    public function edit(Product $product)
    {
        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     */
    #[Put('/{product}/update', name: "products.update")]
    public function update(Request $request, Product $product)
    {
        $product->update(['title' => $request->title, 'sku' => $request->sku, 'price' => $request->price]);

        return redirect()->route('categories.show', $product->category);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product)
    {
        //
    }
}
