<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'seller',
        'link',
        'price',
        'user_id',
        'product_id'
    ]; 


    public function product()
    {
        return $this->belongsTo(\App\Models\Product::class);
    }    
}
