<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'sku',
        'price',
        'user_id',
        'category_id'
    ]; 

    protected $casts = [
        'price' => 'integer',
    ];

    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class);
    }

    public function sellers()
    {
        return $this->hasMany(\App\Models\Seller::class);
    }
}
